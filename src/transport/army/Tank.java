package transport.army;

import transport.Car;

public class Tank extends Car {
	private final   int  MAXSPEED=50;
	int calibre;


	public Tank() {
		super(250);
		System.out.println("Making a Tank");
		this.calibre = 30;
	}

	public void fire(){
		System.out.println("BOOM");
	}

	@Override
	public void pushGas() {
		super.pushGas();
		if (speed > MAXSPEED){
			setSpeed(MAXSPEED);
		}
	}

	public String  toString(){
		return "Tank with calibre "+calibre + " and speed " + speed;
	}

}
