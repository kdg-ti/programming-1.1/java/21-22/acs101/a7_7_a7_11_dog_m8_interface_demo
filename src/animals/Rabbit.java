package animals;

public class Rabbit extends Animal {
    protected boolean digs;
    //constructor
    public Rabbit(String name, String breed, String colour, boolean digs) {
        super(name,breed,colour,"I'm an ice rabbit");
        this.digs=digs;
    }

    //toString

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("rabbit{");
        sb.append("\n digs: ");
        sb.append(digs);
        sb.append("\n");
        sb.append(super.toString());
        sb.append('}');
        return sb.toString();
    }


}
