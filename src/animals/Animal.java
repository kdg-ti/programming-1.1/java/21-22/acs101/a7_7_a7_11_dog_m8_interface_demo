package animals;

public class Animal {
    protected String name;
    protected String breed;
    protected String colour;
    //getters
    protected String tagLine;

    //constructor

    public Animal(String name, String breed, String colour, String tagLine) {
        this.name = name;
        this.breed = breed;
        this.colour = colour;
        this.tagLine =tagLine;
    }

    //methods

        public String getTagline() {
            return tagLine;
        }


    @Override
    public String toString() {
        return "Animal{" +
          "name='" + name + '\'' +
          ", breed='" + breed + '\'' +
          ", colour='" + colour + '\'' +
          ", tagline='" + getTagline() + '\'' +
          '}';
    }

    public void setName(String name) {
        this.name = name;
    }
}

