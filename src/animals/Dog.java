package animals;

import commercial.Product;

public class Dog extends Animal implements Product {
    protected String chipNumber;

//getters


   //constructor

    public Dog(String name, String breed, String colour, String chipNumber) {
        super(name,breed,colour,"Like a dog in a bowling game");
        this.chipNumber=chipNumber;
    }

    public double getVat(){
        return 0.06;
    }

    @Override
    public String toString() {
        return "Dog{" +
          "chipNumber='" + chipNumber + '\'' +
          "} " + super.toString();
    }
}
