package animals;

public class AnimalTest11 {
    public static void main(String[] args) {
        Animal[] animals = {
          new Dog("Jef","collie","black",
            "1111"),
        new Rabbit("Pauline", "Flemish Giant",
          "gray",true),
          new Animal("Animal","muppet","purple","RHAAAA")
        };
        String[] names = {"Sneezy","Doc", "Sly"};
        for (int i = 0; i < animals.length; i++) {
            animals[i].setName(names[i]);
            System.out.println(animals[i].toString());
        }
        System.out.println("Hashcode of \"123\"= " + "123".hashCode());

    }
}
