import animals.Dog;
import commercial.Product;
import media.Radio;
import transport.Car;

public class SalesRunner {
  public static void main(String[] args) {
    Product [] sellMe = {new Dog("Jef","collie","black",
      "1111"), new Car(),new Radio()};
    printall(sellMe);
  }

  public static void printall(Product[] products){
    for (int i = 0; i < products.length; i++) {
      System.out.println("VAT is "+ products[i].getVat() + " for " + products[i]);
    }
  }
}
