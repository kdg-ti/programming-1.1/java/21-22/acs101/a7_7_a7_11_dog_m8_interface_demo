package commercial;

public interface Product {
   double getVat();
}
