package media;

import commercial.Product;

public class Radio implements Product {
	public void play(){
		System.out.println("Listen to the radio");
	}

	@Override
	public double getVat() {
		return 0.12;
	}

	@Override
	public String toString() {
		return "Radio{" +
			"vat=" + getVat() +
			'}';
	}
}
